## MySQL-Redis ##

### 1、什么是 MySQL-Redis ? ###

**MySQL-Redis**是一款可以在 **MySQL** 中操作 **Redis** 的一个**UDF**(**MySQL用户自定义函数插件**)，不能独立运行，需要依赖于 **MySQL**

### 2、支持系统 ###

mysql_redis可以在支持标准C99编译器的操作系统上编译，但是hiredis-vip与Redis的支持环境限制了mysql_redis的适用范围

### 3、MySQL-Redis 可以操作 Redis 集群吗？

可以的， **MySQL-Redis**可以操作单例或者集群**Redis**，操作的唯一的区别就是**MySQL**函数不一样而已

### 4、性能怎么样 ？

性能这个取决于网络， 实例操作Redis速度很快。因为使用C语言开发

### 5、使用场景 ?

可以使用在需要 Redis和MySQL数据一致性的地方。

### 6、MySQL-Redis 函数 ###

- **RedisConnect("127.0.0.1", 6379)**

  单例Redis连接，第二个参数可以省略，默认端口 6379

  对应SQL代码：

  ```
  SELECT RedisConnect("127.0.0.1", 6379);
  ```

- **RedisClusterConnect**

  Redis集群连接

  对应SQL代码：

  ```
  SELECT RedisClusterConnect("127.0.0.1:6379,127.0.0.2:6379");
  ```

  多个机器之间采用`,`隔开

- **RedisConnectFree**

  对应 **RedisConnect()的连接释放**

  操作完成后，如果需要手动释放，需要调用本方法，目前暂未实现连接池，后续版本实现

  对应SQL代码：

  ```
  SELECT RedisConnectFree();
  ```

- **RedisClusterConnectFree**

  对应 **RedisClusterConnect()的连接释放**

  操作完成后，如果需要手动释放，需要调用本方法，目前暂未实现连接池，后续版本实现

  使用方法：

  ```
  SELECT RedisClusterConnectFree();
  ```

- **RedisSet**

  设置键值对的快捷方法

  对应SQL代码：

  ```SQL
  SELECT RedisSet("a", 1); -- 等同于 Redis中的语句： SET a 1
  ```

- **RedisClusterSet**

  设置键值对的集群方法方法，参数等同于 RedisSet函数，见上面

  

- **RedisExecCommand**

  执行Redis语句，包含三个或者两个参数，如下SQL：

  三个参数的SQL方法：

  ```
  SELECT RedisExecCommand("SET", "a", 1);
  ```

  两个参数的SQL方法：

  ```
  SELECT RedisExecCommand("SELECT", 1);
  ```

- **RedisClusterExecCommand**

  集群操作方法，参数等同于上面单例：**RedisExecCommand**

- **RedisRawCommand**

  执行Redis语句，由用户提供一个完整的Redis语句

  SQL代码如下：

  ```
  SELECT RedisRawCommand("SET Hello World");
  ```

- **RedisClusterRawCommand**

  集群模式的**RedisClusterRawCommand**方法，参数等同于 单例的 **RedisRawCommand**
  
- **RedisListIndex**

  获取 Redis List数据结构的某一项，第一项表示 Redis数据结构，第二项表示索引值，如：

  ```
  SELECT RedisListIndex(RedisRawCommand("LRANGE score_lists 0 10"), 1);
  ```

- **RedisListJoin**

  将 Redis List数据结构的所有项通过第二个参数合并为一个字符串，如：

  ```
  SELECT RedisListJoin(RedisRawCommand("LRANGE score_lists 0 10"), "-");
  // 假设 Redis List score_lists结构如下：
  // 0 1 2
  // 那么合并结果：
  // 0-1-2
  ```

## 7、返回值 ##

1：

**RedisSET** 、 **RedisClusterSet** 、**RedisConnect** 、**RedisConnectFree** 、 **RedisClusterConnect** 、 **RedisClusterConnectFree** 成功的情况下返回 1，否则SQL语句错误

2：

**RedisExecCommand** 、 **RedisClusterExecCommand** 、**RedisRawCommand** 、**RedisClusterRawCommand**、**RedisListIndex** 、**RedisListJoin** 返回字符串，具体如下：

- OK

  在执行 Redis SET命令的时候返回 "OK"表示成功，否则就表示错误信息

- 其他字符串

  执行GET 命令的时候 返回具体值

## 8、怎么安装 ?

前提条件：

电脑安装 **Redis** 、**MySQL**(开发机器MySQL 5.7.22)、**hiredis-vip**

[hiredis-vip仓库地址](https://github.com/vipshop/hiredis-vip)

准备好前提条件后，开始MySQL-Redis插件的编译安装过程

1. git clone 代码库

   ```
   git clone  https://gitee.com/josinli/mysql_redis.git
   ```

2. 修改 CMakeLists.txt 文件，因为代码库采用 CMake编译系统：

   ```
   cmake_minimum_required(VERSION 3.13)
   project(udf_redis C)
   
   set(CMAKE_C_STANDARD 99)
   
   include_directories(
       .
       /usr/local/mysql/include/      # 把这行更改为您系统安装mysql的头文件目录
       /usr/local/include/hiredis-vip # 把这行更改为您系统安装hiredis-vip的头文件目录
   )
   
   link_directories(
       /usr/local/lib                 # 把这行更改为您系统的 hiredis-vip的lib安装目录
   )
   
   add_library(udf_redis SHARED mysql_redis.c mysql_redis.h)
   
   target_link_libraries(udf_redis hiredis_vip)
   ```

   

3. 更改完成后，执行下面的操作

   ```
   cd build
   cmake ..
   make 
   ```

4. 可以看到build目录生成了库文件 **libudf_redis.so** 或者 **libudf_redis.dylib**

5. 进入您的MySQL，执行如下SQL语句找到您的MySQL插件目录：

   ```
   SHOW VARIABLES LIKE 'plugin_dir%';
   ```

6. 拷贝上面第四步中的 **libudf_redis.s**o 或者 **libudf_redis.dylib** 到第五步中的目录中

7. 针对您的系统，Linux系统拷贝 **create_function_linux.sql** 或者 OSX系统拷贝 **create_function_osx.sql** 并执行

8. 重启您的MySQL，开始在SQL中愉快的体验Redis吧!



